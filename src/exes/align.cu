#include <fstream>
#include <iostream>
#include <sstream>
#include <chrono>
#include <vector>

#include "glog/logging.h"

#include <cudascan/cudaScan.h>
#include <solver/cudaGICP.h>
#include <common/csv_parser.hpp>
#include <common/transform3f.h>


using namespace gicp;
using namespace std;

int main(int argc, char *argv[]) {
    google::InitGoogleLogging(argv[0]);
	if (argc < 3) {
		cerr << "Usage: ./build-kdtree csv_file_name csv_file_name\n";
		return EXIT_FAILURE;
	}

	string file_name(argv[1]);
	csv_parser parser(file_name);

    float t[6] = {0, 0, 0, 0, 0, 0};
    if (argc>=9) {
        t[3] = atof(argv[3]);
        t[4] = atof(argv[4]);
        t[5] = atof(argv[5]);
        t[0] = atof(argv[6]);
        t[1] = atof(argv[7]);
        t[2] = atof(argv[8]);
    }


	file_name = string(argv[2]);
	csv_parser parser2(file_name);

    int sizeA, sizeB;
    float *a = parser.parseToArray(&sizeA);
    float *b = parser2.parseToArray(&sizeB);

    auto t1 = std::chrono::high_resolution_clock::now();
    cuda_scan csA((vec3f *)a, sizeA);
    cuda_scan csB((vec3f *)b, sizeB);
    auto t2 = std::chrono::high_resolution_clock::now();
    std::cout << "Build took "
              << std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1).count()
              << " milliseconds\n";
    auto t3 = std::chrono::high_resolution_clock::now();
    cuda_gicp gicp(&csA, &csB, t);
    auto t4 = std::chrono::high_resolution_clock::now();
    std::cout << "Ceres took "
              << std::chrono::duration_cast<std::chrono::milliseconds>(t4-t3).count()
              << " milliseconds\n";

    double out[6];
    auto t5 = std::chrono::high_resolution_clock::now();
    gicp.align(out);
    auto t6 = std::chrono::high_resolution_clock::now();
    std::cout << "Align took "
              << std::chrono::duration_cast<std::chrono::milliseconds>(t6-t5).count()
              << " milliseconds\n";

    //std::cout << "T: " << out[3] 
    //              << " " << out[4]
    //              << " " << out[5]
    //              << " " << out[0]
    //              << " " << out[1]
    //              << " " << out[2]
    //              << std::endl;

      printf("sparki  : %5.3f %5.3f %5.3f %5.3f %5.3f %5.3f\n",
            out[3], out[4], out[5],
            out[0], out[1], out[2]);

	return EXIT_SUCCESS;
}
