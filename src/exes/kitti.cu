#include <fstream>
#include <iostream>
#include <sstream>
#include <chrono>
#include <vector>
#include <dirent.h>
#include <stdio.h>


#include <cudascan/cudaScan.h>
//#include <solver/cudaGICPtiny.h>
#include <solver/cudaGICP.h>
#include <common/csv_parser.hpp>
#include <common/transform3f.h>


using namespace gicp;
using namespace std;

#include "calib.h"

typedef struct {

    float *xyz;
    int n_hits;

} velodyne_returns_t;

velodyne_returns_t
read_kitti_file(string fn, int step)
{
    printf("Reading %s\n", fn.c_str());

    FILE *fp = fopen(fn.c_str(), "r");

    fseek(fp, 0L, SEEK_END);
    size_t sz = ftell(fp);

    rewind(fp);

    int n_hits = sz / (sizeof(float)*4) * (9.0-step)/8.0;

    velodyne_returns_t vr;
    vr.xyz = (float*) malloc(sizeof(float)*3*n_hits);
    vr.n_hits = n_hits;

    for (int i=0; i<n_hits; i++) {

        float x, y, z, intens;
        if (fread(&x, sizeof(float), 1, fp) == 0) break;
        if (fread(&y, sizeof(float), 1, fp) == 0) break;
        if (fread(&z, sizeof(float), 1, fp) == 0) break;
        if (fread(&intens, sizeof(float), 1, fp) == 0) break;

        vr.xyz[3*i + 0] = x;
        vr.xyz[3*i + 1] = y;
        vr.xyz[3*i + 2] = z;
    }


    return vr;
}

vector<string>
get_vel_bin_in_dir(char *dir_name) {

    DIR           *d;
    struct dirent *dir;
    d = opendir(dir_name);

    vector<string> vel_sync_fns;

    if (d) {
        while ((dir = readdir(d)) != NULL) {

            // Check to make sure this is a binary file match
            if (strlen(dir->d_name) >= 4 && strcmp(dir->d_name + strlen(dir->d_name)  - 4, ".bin") == 0) {
                vel_sync_fns.push_back(string(dir_name) + "/" + string(dir->d_name));
            }
        }

        closedir(d);
    }

    return vel_sync_fns;
}

int main(int argc, char *argv[]) {
    google::InitGoogleLogging(argv[0]);
    std::cout << "KITTI Test" << std::endl;

	if (argc < 2) {
		cerr << "Usage: ./cugicp-exes-kitti kitti_dir\n";
		return EXIT_FAILURE;
	}

    vector<string> vel_sync_fns = get_vel_bin_in_dir(argv[1]);
    std::sort(vel_sync_fns.begin(), vel_sync_fns.end());

    printf("Have %d velodyne scans\n", vel_sync_fns.size());

    velodyne_returns_t vr1, vr2;
    float t[6] = {0, 0, 0, 0, 0, 0};
    transform3f pose(t);
    transform3f tr00 = get00();
    double out[6];

    int step = 1;
    FILE * pFile;
    FILE * pose_file;
    char name [50];
    sprintf(name, "align%d.csv", step);
    pFile = fopen (name,"w");
    pose_file = fopen("00.txt", "w");
    for (int i=0; i<200; i++) {

        vr1 = read_kitti_file(vel_sync_fns[i+1], step);
        vr2 = read_kitti_file(vel_sync_fns[i], step);
        std::cout << "number of points: " << vr1.n_hits << std::endl;

        cuda_scan csA((vec3f *)vr1.xyz, vr1.n_hits);
        auto t1 = std::chrono::high_resolution_clock::now();
        cuda_scan csB((vec3f *)vr2.xyz, vr2.n_hits);

        auto t2 = std::chrono::high_resolution_clock::now();
        cuda_gicp gicp(&csA, &csB, t);

        gicp.align(out);
        auto t6 = std::chrono::high_resolution_clock::now();
        std::cout << "cuGICP took "
                  << std::chrono::duration_cast<std::chrono::milliseconds>(t6-t1).count()
                  << " milliseconds\n";
        std::cout << "Optimization took "
                  <<
                  std::chrono::duration_cast<std::chrono::milliseconds>(t6-t2).count()
                  << " milliseconds\n";
        std::cout << "T: " << out[3] 
                  << " " << out[4]
                  << " " << out[5]
                  << " " << out[0]
                  << " " << out[1]
                  << " " << out[2]
                  << std::endl;
        std::copy(out, out+6, t);
        int gpu =
            std::chrono::duration_cast<std::chrono::milliseconds>(t6-t1).count();

        fprintf(pFile, "%d, %d, %f, %f, %f, %f, %f, %f\n",
                vr1.n_hits, gpu, out[3], out[4], out[5], out[0], out[1], out[2]);

        transform3f temp(t);
        transform3f temp2;
        temp2.copy(tr00);
        temp2.transform(temp);
        temp2.transform(pose);
        pose.copy(temp2);
        fprintf(pose_file, "%f %f %f %f %f %f %f %f %f %f %f %f\n",
            pose.m[0][0], pose.m[0][1], pose.m[0][2], pose.m[0][3],
            pose.m[1][0], pose.m[1][1], pose.m[1][2], pose.m[1][3],
            pose.m[2][0], pose.m[2][1], pose.m[2][2], pose.m[2][3]);

        free(vr1.xyz);
        free(vr2.xyz);

    }

	return EXIT_SUCCESS;
}
