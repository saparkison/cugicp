#include <fstream>
#include <iostream>
#include <sstream>
#include <chrono>

#include <Eigen/Core>
#include <Eigen/Eigenvalues> 
#include "glog/logging.h"

#include <common/vec3f.h>
#include <common/mat3f.h>
#include <common/eigenDecomp.h>

using namespace gicp;

int main(int argc, char *argv[]) {

    mat3f out;
    out.set(1.0);

    vec3f v1,v2,v3;
    v1.v[0] = 1.0; v1.v[1] = 2.0; v1.v[2] = -2.0;
    v2.v[0] = 4.0; v2.v[1] = 2.0; v2.v[2] = 3.0;
    v3.v[0] = -2.0; v3.v[1] = 0.0; v3.v[2] = 1.0;
    out = out + outer_product(v1, v1);
    out = out + outer_product(v2, v2);
    out = out + outer_product(v3, v3);

    std::cout << "Orginial Matrxi: \n";
    printMat(out);

    mat3f eigVals, eigVec;
    //***C++11 Style:***
    auto begin = std::chrono::steady_clock::now();

    eigenDecomp(out, eigVals, eigVec);
    auto end= std::chrono::steady_clock::now();
    std::cout << "My time = " <<
      std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count()
      <<std::endl;

    std::cout << "Eigen Value Matrix: \n";
    printMat(eigVals);

    std::cout << "Eigen Vector Matrix: \n";
    printMat(eigVec);

    mat3f recon;
    recon = matrix_multiply(matrix_multiply(trans(eigVec),eigVals),eigVec);

    std::cout << "Reconstructed Matrxi: \n";
    printMat(recon);

    std::cout << "Matrix Multiplication: \n";
    printMat(matrix_multiply(out, out));

    Eigen::Vector3f ev1;
    ev1 << 1.0, 2.0 -2.0;
    Eigen::Vector3f ev2;
    ev2 << 4.0, 2.0, 3.0;
    Eigen::Vector3f ev3;
    ev3 << -2.0, 0.0, 1.0;
    Eigen::Matrix3f A =
      ev1*ev1.transpose()+ev2*ev2.transpose()+ev3*ev3.transpose();
    Eigen::SelfAdjointEigenSolver<Eigen::Matrix3f> es(3);

    begin = std::chrono::steady_clock::now();
    es.computeDirect(A);
    end = std::chrono::steady_clock::now();
    std::cout << "Eigens' time = " <<
      std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count()
      <<std::endl;
	return EXIT_SUCCESS;
}
