
#define THREADNUM 128

#define CUDA_STACK 50
#include"cudaScan.h"

#include<chrono>

#include<common/eigenDecomp.h>
#include <thrust/transform.h>
#include <thrust/functional.h>
#include <thrust/execution_policy.h>

#define cudaSafe(ans) { gpuAssert((ans), __FILE__, __LINE__, true); }

namespace gicp {

struct transformNNStruct {
    transform3f t;

    __host__ __device__
    transformNNStruct(float v[6]) : t(v) {
    };

    __host__ __device__
    ~transformNNStruct() {
    };

    __host__ __device__
    NNunit operator()(const vec3f &in, const NNunit &u) {
        NNunit out;

        out.pointAorg = in;
        out.pointA = t(in);
        out.covA = u.covA;

        out.pointB = u.pointB;
        out.covB = u.covB;

        out.inRange = u.inRange;

        return out;
    };

    __host__ __device__
    NNunit operator()(const mat3f &in, const NNunit &u) {
        NNunit out;

        out.pointAorg = u.pointAorg;
        out.pointA = u.pointA;
        out.covA = t(in);

        out.pointB = u.pointB;
        out.covB = u.covB;

        out.inRange = u.inRange;

        return out;
    };
};


struct CostStruct {
    float cost;
    vec3f tgrad;
    vec3f rgrad;
    int numValid;
};

__host__ __device__
CostStruct operator+(const CostStruct &a, const CostStruct &b) {
    CostStruct out;
    out.cost = a.cost+b.cost;
    out.tgrad = a.tgrad+b.tgrad;
    out.rgrad = a.rgrad+b.rgrad;
    out.numValid = a.numValid+b.numValid;
    return out;
};

struct MahalCost {
    float t[6];
    bool compute_jacobian_;

    MahalCost(const float v[6], const bool &compute_jacobian) {
        compute_jacobian_ = compute_jacobian;
        t[0] = v[0];
        t[1] = v[1];
        t[2] = v[2];
        t[3] = v[3];
        t[4] = v[4];
        t[5] = v[5];
    }

    __host__ __device__
    CostStruct operator()(const NNunit &u) {
        CostStruct out;
        if(u.inRange) {
            out.numValid = 1;
            mat3f m = u.covA+u.covB;
            matInv(m);
            vec3f d = u.pointA-u.pointB;

            vec3f temp = right_mult(m,d);
            out.cost = inner_product(temp,d);
            if(compute_jacobian_) {
              out.tgrad = temp;
              mat3f rgradM = outer_product(temp, u.pointAorg);
              out.rgrad = compute_dr(rgradM, t);
            }
        } else {
            out.cost = 0 ;
            out.tgrad.set(0);
            out.rgrad.set(0);
            out.numValid = 0;
        }
        return out;
    }
};

void gpuAssert(cudaError_t code, const char *file, int line, bool abort) {
    if (code != cudaSuccess) {
        fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
        if (abort) exit(code);
    }
}

__device__ __inline__ void
cov_calc(vec3f *points, int *k_nn, mat3f *cov, int k, int pos){
    vec3f mean;
    mean.set(0);

    for(int i = 0; i<k; i++) {
        /*
        if(pos==60420){
            printf("knn : %d ", k_nn[i]);
            printVec(points[k_nn[i]]);
        }
        */
        mean=mean+points[k_nn[i]];
    }
    mean.scalar_mult(1.0/(float)(k));

    cov[0].set(0.0);
    for(int i = 0; i<k; i++) {
        vec3f temp = points[k_nn[i]]-mean;
        cov[0]= cov[0]+outer_product(temp, temp);
    }
    cov[0].scalar_mult(1.0/(float)(k-1));

    /* PCA */
    mat3f eVecs, eVals;
    eigenDecomp(cov[0], eVals, eVecs);
    float min = eVals.m[0][0];
    int minIdx = 0;
    if(min>eVals.m[1][1]) {
        min = eVals.m[1][1];
        minIdx = 1;
    }
    if(min>eVals.m[2][2]) {
        minIdx = 2;
    }
    for(int it =0; it < 3; it++){
        if(it == minIdx) {
            eVals.m[it][it] = 1e-4;
        } else {
            eVals.m[it][it] = 1.0;
        }
    }
    cov[0] = matrix_multiply(matrix_multiply(trans(eVecs),eVals),eVecs);
}


__global__ void
compute_covs(vec3f *points, int *k_nns, mat3f *covs, int n, int k){
    int pos = blockIdx.x*blockDim.x+threadIdx.x;
    if(pos>=n) return;

    cov_calc(points, k_nns+pos*k, covs+pos, k, pos);

   /* 
    if(pos==60420){
        printf("COV %d, n %d:\n %f, %f, %f\n %f, %f, %f\n %f, %f, %f\n", pos, n,
                covs[pos].m[0][0], covs[pos].m[0][1], covs[pos].m[0][2],
                covs[pos].m[1][0], covs[pos].m[1][1], covs[pos].m[1][2],
                covs[pos].m[2][0], covs[pos].m[2][1], covs[pos].m[2][2]);
    }
    */
    return;
}

__global__ void
populate_nn(int *nn, vec3f *points, mat3f *covs, NNunit *nnunits_in, int n){
    int pos = blockIdx.x*blockDim.x+threadIdx.x;
    if(pos>=n) return;

    nnunits_in[pos].pointB = points[nn[pos]];
    nnunits_in[pos].covB = covs[nn[pos]];

    vec3f res = nnunits_in[pos].pointB-nnunits_in[pos].pointA;
    double d = sqrt(pow(res.v[0],2)+pow(res.v[1],2)+pow(res.v[2],2));

    if(d<3.0) {
        nnunits_in[pos].inRange=true;
    } else {
        nnunits_in[pos].inRange=false;
    }
    /*
    if(pos==60420){
        printf("COV %d, nn %d:\n %f, %f, %f\n %f, %f, %f\n %f, %f, %f\n", pos, nn[pos],
                nn_covs[pos].m[0][0], nn_covs[pos].m[0][1], nn_covs[pos].m[0][2],
                nn_covs[pos].m[1][0], nn_covs[pos].m[1][1], nn_covs[pos].m[1][2],
                nn_covs[pos].m[2][0], nn_covs[pos].m[2][1], nn_covs[pos].m[2][2]);
    }
    */
}


cuda_scan::cuda_scan(vec3f *points, int n){
    _m_n = n;

    //cudaMalloc((void **)&_m_d_points, sizeof(vec3f)*_m_n);
    //cudaMemcpy(_m_d_points, points, sizeof(vec3f)*_m_n, cudaMemcpyHostToDevice);
    cudaStream_t stream_1, stream_2;
    cudaStreamCreate(&stream_1);
    cudaStreamCreate(&stream_2);

    auto t1 = std::chrono::high_resolution_clock::now();
    _m_kd_tree = new cuda_kd_tree(3);
    _m_d_points = _m_kd_tree->build_tree(points, _m_n);
    auto t2 = std::chrono::high_resolution_clock::now();
    std::cout << "tree build " << "time "<< 
    std::chrono::duration_cast<std::chrono::microseconds>(t2-t1).count()
    <<std::endl;

    cudaSafe(cudaMalloc((void **)&_m_d_points_org, sizeof(vec3f)*_m_n));
    cudaSafe(cudaMemcpyAsync(_m_d_points_org, _m_d_points, sizeof(vec3f)*_m_n,
        cudaMemcpyDeviceToDevice, stream_1));

    t1 = std::chrono::high_resolution_clock::now();
    int *k_nns = _m_kd_tree->find_knn(_m_d_points, _m_n, 12);
    t2 = std::chrono::high_resolution_clock::now();
    std::cout << "knn search " << "time "<< 
    std::chrono::duration_cast<std::chrono::microseconds>(t2-t1).count()
    <<std::endl;

    t1 = std::chrono::high_resolution_clock::now();
    cudaSafe(cudaMalloc((void **)&_m_d_covs, sizeof(mat3f)*_m_n));
    int blocks = ceil((float)_m_n/THREADNUM);
    compute_covs<<<blocks, THREADNUM>>>(_m_d_points, k_nns, _m_d_covs, n, 12);
    cudaSafe(cudaDeviceSynchronize());
    t2 = std::chrono::high_resolution_clock::now();
    std::cout << "cov calc " << "time "<< 
    std::chrono::duration_cast<std::chrono::microseconds>(t2-t1).count()
    <<std::endl;

    cudaSafe(cudaMalloc((void **)&_m_d_covs_org, sizeof(mat3f)*_m_n));
    cudaSafe(cudaMemcpyAsync(_m_d_covs_org, _m_d_covs, sizeof(mat3f)*_m_n,
        cudaMemcpyDeviceToDevice, stream_2));
    cudaSafe(cudaMalloc((void **)&_m_d_nn_points, sizeof(vec3f)*_m_n));
    cudaSafe(cudaMalloc((void **)&_m_d_nn_covs, sizeof(mat3f)*_m_n));

    cudaSafe(cudaMalloc((void **)&d_nnunits_, sizeof(NNunit)*_m_n));
}

cuda_scan::~cuda_scan() {
    cudaSafe(cudaFree(_m_d_covs));
    cudaSafe(cudaFree(_m_d_points_org));
    cudaSafe(cudaFree(_m_d_covs_org));
    cudaSafe(cudaFree(_m_d_nn_points));
    cudaSafe(cudaFree(_m_d_nn_covs));
    cudaSafe(cudaFree(d_nnunits_));
    delete _m_kd_tree;
}

void
cuda_scan::transform(float *t) {
    memcpy(_m_t, t, sizeof(float)*6);
    cudaStream_t stream_1, stream_2, stream_3, stream_4;
    cudaStreamCreate(&stream_1);
    cudaStreamCreate(&stream_2);
    cudaStreamCreate(&stream_3);
    cudaStreamCreate(&stream_4);
    thrust::transform(thrust::cuda::par.on(stream_1), _m_d_points_org,
        _m_d_points_org + _m_n, _m_d_points, transform3f(t));
    thrust::transform(thrust::cuda::par.on(stream_2), _m_d_covs_org,
        _m_d_covs_org + _m_n, _m_d_covs, transform3f(t));
    thrust::transform(thrust::cuda::par.on(stream_3), _m_d_points_org, _m_d_points_org + _m_n,
            d_nnunits_, d_nnunits_, transformNNStruct(t));
    thrust::transform(thrust::cuda::par.on(stream_4), _m_d_covs_org, _m_d_covs_org + _m_n,
            d_nnunits_, d_nnunits_, transformNNStruct(t));
    cudaSafe(cudaDeviceSynchronize());
}

void
cuda_scan::transformNN(float *t) {
    memcpy(_m_t_org, t, sizeof(float)*6);
    thrust::transform(thrust::device, _m_d_points_org, _m_d_points_org + _m_n,
            d_nnunits_, d_nnunits_, transformNNStruct(t));
    cudaSafe(cudaDeviceSynchronize());
    //thrust::transform(thrust::device, _m_d_covs_org, _m_d_covs_org + _m_n,
    //        d_nnunits_, d_nnunits_, transformNNStruct(t));
}

void
cuda_scan::getNN(vec3f *d_querypoints, int n, NNunit *nnunits_in) {
    int *nn = _m_kd_tree->find_nn(d_querypoints, n);

    int blocks = ceil((float)n/THREADNUM);
    populate_nn<<<blocks, THREADNUM>>>(nn, _m_d_points, _m_d_covs, nnunits_in, n);
    cudaSafe(cudaDeviceSynchronize());
}

void
cuda_scan::alignToScan(cuda_scan *alignCS) {

    alignCS->getNN(_m_d_points, _m_n, d_nnunits_);
}

float
cuda_scan::calcMalhalDist(float *grad, bool compute_jacobian) {

    CostStruct c;
    c.cost = 0;
    c.tgrad.set(0);
    c.rgrad.set(0);
    c = thrust::transform_reduce(thrust::device, d_nnunits_, d_nnunits_+_m_n,
            MahalCost(_m_t_org, compute_jacobian), c, thrust::plus<CostStruct>());

    grad[0] = c.rgrad.v[0]*2.0f/c.numValid;
    grad[1] = c.rgrad.v[1]*2.0f/c.numValid;
    grad[2] = c.rgrad.v[2]*2.0f/c.numValid;
    grad[3] = c.tgrad.v[0]*2.0f/c.numValid;
    grad[4] = c.tgrad.v[1]*2.0f/c.numValid;
    grad[5] = c.tgrad.v[2]*2.0f/c.numValid;
    return c.cost/c.numValid;

}

}

