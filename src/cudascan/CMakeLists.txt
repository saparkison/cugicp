set (LIB_NAME cudascan)
set (REQUIRED_LIBS gtest pthread ceres glog qhull cudakdtree)
find_package(CUDA)
set (CUDA_NVCC_FLAGS "${CUDA_NVCC_FLAGS} -Wno-deprecated-gpu-targets -lgsl
-lgslcblas -lm -O3 -use_fast_math")

file (GLOB HEADER_FILES *.h **/*.hpp)
set (SRC
    cudaScan.cu
    cudaScan.h
    )

cuda_add_library (${LIB_NAME} ${SRC} SHARED)
target_link_libraries(${LIB_NAME} ${REQUIRED_LIBS})

pods_install_libraries(${LIB_NAME})
pods_install_headers (${HEADER_FILES} DESTINATION ${LIB_NAME})
pods_install_pkg_config_file(${LIB_NAME}
    LIBS -l${LIB_NAME} ${REQUIRED_LIBS}
    REQUIRES ${REQUIRED_PACAKGES}
    VERSION 0.0.1)
