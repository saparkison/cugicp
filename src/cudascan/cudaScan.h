#ifndef __CUDA_SCAN_H__
#define __CUDA_SCAN_H__

#include<common/vec3f.h>
#include<common/mat3f.h>
#include<common/transform3f.h>
#include<cudakdtree/cudaKDtree.h>

namespace gicp {

struct NNunit {
    vec3f pointAorg;
    vec3f pointA;
    vec3f pointB;

    mat3f covA;
    mat3f covB;

    bool inRange;

    __host__ __device__
    NNunit& operator=(const NNunit &other) {
        pointAorg = other.pointAorg;
        pointA = other.pointA;
        pointB = other.pointB;
        covA = other.covA;
        covB = other.covB;
        inRange = other.inRange;
        return *this;
    }

};

class cuda_scan {
    private:
        vec3f *_m_d_points;
        vec3f *_m_d_points_org;
        mat3f *_m_d_covs;
        mat3f *_m_d_covs_org;
        int _m_n;

        NNunit *d_nnunits_;

        vec3f *_m_d_nn_points;
        mat3f *_m_d_nn_covs;
        int _m_align_n;

        float _m_t_org[6];
        float _m_t[6];

        cuda_kd_tree *_m_kd_tree;

    public:
        cuda_scan(vec3f *points, int n);
        ~cuda_scan();

        void transform(float *t);
        void transformNN(float *t);
        //float mahalanomis(cuda_scan *cs);

        void getNN(vec3f *d_querypoints, int n, NNunit *nnunits_in);
        void alignToScan(cuda_scan *alignCS);
        float calcMalhalDist(float *grad, bool compute_jacobian);
};

}

#endif /* CUDA SCAN H */
