#ifndef __GICP_PROBLEM_NNLS_H__
#define __GICP_PROBLEM_NNLS_H__

#include<algorithm>
#include<iostream>

#include<ceres/ceres.h>

#include<cudascan/cudaScan.h>

namespace gicp {

class GicpProblemNNLS : public ceres::SizedCostFunction<1, 6>{
  public:
        GicpProblemNNLS(cuda_scan *A, cuda_scan *B, float initT[6]) {
            _A = A;
            _B = B;
            _A->transform(initT);
            _A->alignToScan(_B);
        }

        virtual bool Evaluate(double const* const* parameters,
                              double *residuals,
                              double **jacobian) const {
 
            //std::cout << "Parameters: " << parameters[0] << parameters[1] << parameters[2]
            //  << parameters[3] << parameters[4] << parameters[5] << std::endl;
            float t[6];
            t[0] = parameters[0][0];
            t[1] = parameters[0][1];
            t[2] = parameters[0][2];
            t[3] = parameters[0][3];
            t[4] = parameters[0][4];
            t[5] = parameters[0][5];

            bool compute_jacobian = false;
            if(jacobian!=NULL && jacobian[0] != NULL) 
              compute_jacobian = true;
            //    std::cout << "Initial x: " << t[0] << 
            //                     " , " << t[1] <<
            //                     " , " << t[2] <<
            //                     " , " << t[3] <<
            //                     " , " << t[4] <<
            //                     " , " << t[5] <<
            //                     "\n";
            auto t1 = std::chrono::high_resolution_clock::now();
            _A->transformNN(t);
            auto t2 = std::chrono::high_resolution_clock::now();
            //std::cout << "transform took "
            //          << std::chrono::duration_cast<std::chrono::microseconds>(t2-t1).count()
            //          << " microseconds\n";
            float tempG[6];
            auto t3 = std::chrono::high_resolution_clock::now();
            float f = _A->calcMalhalDist(tempG, compute_jacobian);
            auto t4 = std::chrono::high_resolution_clock::now();
            //std::cout << "Cost Calculation took "
            //          << std::chrono::duration_cast<std::chrono::microseconds>(t4-t3).count()
            //          << " microseconds\n";
            //std::cout <<" F: " << f << std::endl;
            residuals[0] = static_cast<double>(f);
            //std::cout <<" Residual: " << residuals[0] << std::endl;
            if(compute_jacobian) {
              jacobian[0][0] = tempG[0];
              jacobian[0][1] = tempG[1];
              jacobian[0][2] = tempG[2];
              jacobian[0][3] = tempG[3];
              jacobian[0][4] = tempG[4];
              jacobian[0][5] = tempG[5];
            }

            return true;
        }

        cuda_scan *_A;
        cuda_scan *_B;
};

}

#endif
