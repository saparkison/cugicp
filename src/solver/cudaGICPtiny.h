#ifndef __CUDA_GICP_TINY_H__
#define __CUDA_GICP_TINY_H__

#include<algorithm>
#include<iostream>
#include<cfloat>

#include<ceres/tiny_solver.h>
#include<ceres/tiny_solver_cost_function_adapter.h>

#include<common/vec3f.h>
#include<cudascan/cudaScan.h>
#include<solver/gicpProblemTiny.h>

namespace gicp {

class cuda_gicp_tiny {
    private:
        cuda_scan *_A;
        cuda_scan *_B;
        float _t[6];

    public:
        cuda_gicp_tiny(cuda_scan *A, cuda_scan *B, float initT[6]) {
            _A = A;
            _B = B;
            std::copy(initT, initT + 6, _t);
        }

        ~cuda_gicp_tiny() {}

        bool align(double *t) {
            bool success = true;
            float paramChange = FLT_MAX;
            int it = 0;
            Eigen::Matrix<double, 6, 1> parameters;
            double residual = 0;
            while(paramChange>5e-4 && success) {
                std::copy(_t, _t+6, parameters.data());

                ceres::TinySolver<GicpProblemTiny>::Summary summary;

                GicpProblemTiny gp(_A, _B, _t);

                ceres::TinySolver<GicpProblemTiny> solver;
                solver.options.initial_trust_region_radius = 1e-1;
                solver.options.max_num_iterations = 200;
                solver.options.gradient_tolerance =
                    std::numeric_limits<double>::epsilon();
                solver.options.parameter_tolerance =
                    std::numeric_limits<double>::epsilon();
                summary = solver.Solve(gp, &parameters);

                std::cout << "Status: " << summary.status << "\n";
                std::cout << "Initial Cost: " << summary.initial_cost << "\n";
                std::cout << "Final cost: " << summary.final_cost << "\n";
                std::cout << "Initial x: " << _t[0] <<
                                 " , " << _t[1] <<
                                 " , " << _t[2] <<
                                 " , " << _t[3] <<
                                 " , " << _t[4] <<
                                 " , " << _t[5] <<
                                 "\n";
                std::cout << "Final   x: " << parameters[0] <<
                                 " , " << parameters[1] <<
                                 " , " << parameters[2] <<
                                 " , " << parameters[3] <<
                                 " , " << parameters[4] <<
                                 " , " << parameters[5] <<
                                 "\n";

                paramChange=sqrt(pow(parameters[3]-_t[3],2)+pow(parameters[4]-_t[4],2)+pow(parameters[5]-_t[5],2));
                std::copy(parameters.data(), parameters.data() +6, _t);
                //costChange = summary.initial_cost-summary.final_cost;
                it++;
            }
            std::copy(parameters.data(), parameters.data() +6, t);
            std::cout << "Cuda GICP Itterations " << it << std::endl;
            return success;
        }
};

}
#endif
