#ifndef __CUDA_GICP_H__
#define __CUDA_GICP_H__

#include<algorithm>
#include<iostream>
#include<cfloat>

#include<ceres/gradient_checker.h>

#include<common/vec3f.h>
#include<cudascan/cudaScan.h>
#include<solver/gicpProblemNNLS.h>
#include<solver/sqloss.h>


namespace gicp {

class cuda_gicp {
    private:
        cuda_scan *_A;
        cuda_scan *_B;
        float _t[6];

    public:
        cuda_gicp(cuda_scan *A, cuda_scan *B, float initT[6]) {
            _A = A;
            _B = B;
            std::copy(initT, initT + 6, _t);
        }

        ~cuda_gicp() {}

        bool align(double *t) {
            bool success = true;
            float paramChange = FLT_MAX;
            int it = 0;
            double parameters[6];
            while(paramChange>5e-4 && success) {
                std::copy(_t, _t+6, parameters);

                ceres::Solver::Options options;
                options.minimizer_progress_to_stdout = false;
                options.minimizer_type = ceres::LINE_SEARCH;
                options.line_search_direction_type = ceres::BFGS;
                options.parameter_tolerance = 1e-4;
                options.gradient_tolerance = 5e-5;

                ceres::Solver::Summary summary;
                ceres::Problem problem;
                problem.AddParameterBlock(parameters, 6, NULL);
                GicpProblemNNLS *cf = new GicpProblemNNLS(_A, _B, _t);
                problem.AddResidualBlock(cf, new SQLoss(), parameters);

                /*
                std::vector<double *> parameter_blocks;
                parameter_blocks.push_back(parameters);
                ceres::NumericDiffOptions numeric_diff_options;
                numeric_diff_options.relative_step_size = 1e-5;
                ceres::GradientChecker gradient_checker(cf, NULL, numeric_diff_options);
                ceres::GradientChecker::ProbeResults results;
                if (!gradient_checker.Probe(parameter_blocks.data(), 1e-9, &results)) {
                      LOG(ERROR) << "An error has occurred:\n" << results.error_log;
                }
                */
                ceres::Solve(options, &problem, &summary);

                /*
                std::cout << summary.BriefReport() << "\n";
                std::cout << "Initial x: " << _t[0] << 
                                 " , " << _t[1] <<
                                 " , " << _t[2] <<
                                 " , " << _t[3] <<
                                 " , " << _t[4] <<
                                 " , " << _t[5] <<
                                 "\n";
                std::cout << "Final   x: " << parameters[0] <<
                                 " , " << parameters[1] <<
                                 " , " << parameters[2] <<
                                 " , " << parameters[3] <<
                                 " , " << parameters[4] <<
                                 " , " << parameters[5] <<
                                 "\n";
                                 */

                paramChange=sqrt(pow(parameters[3]-_t[3],2)+pow(parameters[4]-_t[4],2)+pow(parameters[5]-_t[5],2));
                std::copy(parameters, parameters +6, _t);
                //costChange = summary.initial_cost-summary.final_cost;
                success = summary.IsSolutionUsable();
                it++;
            }
            std::copy(parameters, parameters +6, t);
            std::cout << "Cuda GICP Itterations " << it << std::endl;
            return success;
        }
};

}
#endif
