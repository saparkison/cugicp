#ifndef __GICP_PROBLEM_Tiny_H__
#define __GICP_PROBLEM_Tiny_H__

#include<algorithm>
#include<iostream>

#include<ceres/ceres.h>

#include<cudascan/cudaScan.h>

namespace gicp {

class GicpProblemNNLS : public ceres::SizedCostFunction<1, 6>{
  public:
        GicpProblemNNLS(cuda_scan *A, cuda_scan *B, float initT[6]) {
            _A = A;
            _B = B;
            _A->transform(initT);
            _A->alignToScan(_B);
        }

        virtual bool Evaluate(double const* const* parameters,
                              double *residuals,
                              double **jacobian) const {
 
            //std::cout << "Parameters: " << parameters[0] << parameters[1] << parameters[2]
            //  << parameters[3] << parameters[4] << parameters[5] << std::endl;
            float t[6];
            std::copy(&parameters[0][0], &parameters[0][0] + 6, t);
            auto t1 = std::chrono::high_resolution_clock::now();
            _A->transformNN(t);
            auto t2 = std::chrono::high_resolution_clock::now();
            std::cout << "transform took "
                      << std::chrono::duration_cast<std::chrono::microseconds>(t2-t1).count()
                      << " microseconds\n";
            float tempG[6];
            auto t3 = std::chrono::high_resolution_clock::now();
            float f = _A->calcMalhalDist(tempG);
            auto t4 = std::chrono::high_resolution_clock::now();
            std::cout << "Cost Calculation took "
                      << std::chrono::duration_cast<std::chrono::microseconds>(t4-t3).count()
                      << " microseconds\n";
            std::cout <<" F: " << f << std::endl;
            residuals[0] = static_cast<double>(f);
            if(jacobian!=NULL && jacobian[0] != NULL) {
                std::copy(tempG, tempG + 6, &jacobian[0][0]);
            }

            return true;
        }

        cuda_scan *_A;
        cuda_scan *_B;
};

}

#endif
