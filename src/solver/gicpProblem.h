#ifndef __GICP_PROBLEM_H__
#define __GICP_PROBLEM_H__

#include<algorithm>
#include<iostream>

#include<ceres/ceres.h>

#include<cudascan/cudaScan.h>

namespace gicp {

class gicp_problem : public ceres::FirstOrderFunction {
    public:
        gicp_problem(cuda_scan *A, cuda_scan *B, float initT[6]) {
            _A = A;
            _B = B;
            _A->transform(initT);
            _A->alignToScan(_B);
        }

        virtual ~gicp_problem() {}

        virtual bool Evaluate(const double *parameters,
                              double *cost,
                              double *gradient) const {

            float t[6];
            std::copy(parameters, parameters + 6, t);
            auto t1 = std::chrono::high_resolution_clock::now();
            _A->transformNN(t);
            auto t2 = std::chrono::high_resolution_clock::now();
            //std::cout << "transform took "
            //          << std::chrono::duration_cast<std::chrono::microseconds>(t2-t1).count()
            //          << " microseconds\n";
            float tempG[6];
            auto t3 = std::chrono::high_resolution_clock::now();
            float f = _A->calcMalhalDist(tempG);
            auto t4 = std::chrono::high_resolution_clock::now();
            //std::cout << "Cost Calculation took "
            //          << std::chrono::duration_cast<std::chrono::microseconds>(t4-t3).count()
            //          << " microseconds\n";
            //std::cout <<" F: " << f << std::endl;
            cost[0] = f;
            if(gradient!=NULL) {
                std::copy(tempG, tempG + 6, gradient);
            }

            return true;
        }

        virtual int NumParameters() const {return 6;}

    private:
        cuda_scan *_A;
        cuda_scan *_B;
};

}

#endif
