#ifndef __SQ_LOSS_H__
#define __SQ_LOSS_H__

#include<ceres/loss_function.h>

#include<cmath>

namespace gicp {

class SQLoss : public ceres::LossFunction {
  public:
    void Evaluate(double s, double rho[3]) const {
     rho[0] = std::sqrt(s);
     rho[1] = 1.0/(2.0*std::sqrt(s));
     rho[2] = -1.0/(4.0*std::pow(s,1.5));
    }
};

}
#endif // _SQ_LOSS_H_
