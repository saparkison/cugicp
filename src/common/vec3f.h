#ifndef __VEC3F_H__
#define __VEC3F_H__

#include<math.h>
#include<stdio.h>

#include<common/mat3f.h>

namespace gicp {

struct vec3f {
    float v[3];
    __host__ __device__ __forceinline__
    void set(float x){
        v[0] =x;
        v[1] =x;
        v[2] =x;
    };
    __host__ __device__ __forceinline__
    void scalar_mult(float s) {
        v[0]*=s;
        v[1]*=s;
        v[2]*=s;
    };
    __host__ __device__ __forceinline__
    vec3f& operator=(const vec3f &other) {
        v[0] = other.v[0];
        v[1] = other.v[1];
        v[2] = other.v[2];
        return *this;
    }

};


__host__ __device__ __forceinline__
vec3f operator-(const vec3f &a, const vec3f &b){
    vec3f out;
    out.v[0] = a.v[0]-b.v[0];
    out.v[1] = a.v[1]-b.v[1];
    out.v[2] = a.v[2]-b.v[2];
    return out;
};
__host__ __device__ __forceinline__

vec3f operator+(const vec3f &a, const vec3f &b){
    vec3f out;
    out.v[0] = a.v[0]+b.v[0];
    out.v[1] = a.v[1]+b.v[1];
    out.v[2] = a.v[2]+b.v[2];
    return out;
};
__host__ __device__ __forceinline__
mat3f
outer_product(const vec3f &a, const vec3f &b){
    mat3f out;
    out.m[0][0] = a.v[0]*b.v[0];
    out.m[0][1] = a.v[0]*b.v[1];
    out.m[0][2] = a.v[0]*b.v[2];
    out.m[1][0] = a.v[1]*b.v[0];
    out.m[1][1] = a.v[1]*b.v[1];
    out.m[1][2] = a.v[1]*b.v[2];
    out.m[2][0] = a.v[2]*b.v[0];
    out.m[2][1] = a.v[2]*b.v[1];
    out.m[2][2] = a.v[2]*b.v[2];
    return out;
};
__host__ __device__ __forceinline__
float
inner_product(const vec3f &a, const vec3f &b){
    return a.v[0]*b.v[0]+a.v[1]*b.v[1]+a.v[2]*b.v[2];
};
__host__ __device__ __forceinline__
vec3f
left_mult(const vec3f &v, const mat3f &m){
    vec3f out;
    out.v[0] = v.v[0]*m.m[0][0] + v.v[1]*m.m[1][0] + v.v[2]*m.m[2][0];
    out.v[1] = v.v[0]*m.m[0][1] + v.v[1]*m.m[1][1] + v.v[2]*m.m[2][1];
    out.v[2] = v.v[0]*m.m[0][2] + v.v[1]*m.m[1][2] + v.v[2]*m.m[2][2];
    return out;
};
__host__ __device__ __forceinline__
vec3f
right_mult(const mat3f &m, const vec3f &v){
    vec3f out;
    out.v[0] = v.v[0]*m.m[0][0] + v.v[1]*m.m[0][1] + v.v[2]*m.m[0][2];
    out.v[1] = v.v[0]*m.m[1][0] + v.v[1]*m.m[1][1] + v.v[2]*m.m[1][2];
    out.v[2] = v.v[0]*m.m[2][0] + v.v[1]*m.m[2][1] + v.v[2]*m.m[2][2];
    return out;
};
__host__ __device__ __forceinline__
int
biggestAxis(const vec3f &v) {
    if(v.v[0]>v.v[1]&&v.v[0]>v.v[2])
        return 0;
    if(v.v[1]>v.v[0]&&v.v[1]>v.v[2])
        return 1;
    if(v.v[2]>v.v[1]&&v.v[2]>v.v[0])
        return 2;
};
struct residual{
    __host__ __device__ vec3f operator()(const vec3f &a, const vec3f &b) {
        return a-b;
    }
};
struct rightv{
    __host__ __device__ vec3f operator()(const mat3f &m, const vec3f &v) {
        return right_mult(m, v);
    }
};
struct dotProd{
    __host__ __device__ float operator()(const vec3f &a, const vec3f &b) {
        return inner_product(a, b);
    }
};
struct outerProd{
    __host__ __device__ mat3f operator()(const vec3f &a, const vec3f &b) {
        return outer_product(a, b);
    }
};
__host__ __device__ vec3f vecMax(const vec3f &a, const vec3f &b) {
        vec3f out;
        out.v[0] = max(a.v[0], b.v[0]);
        out.v[1] = max(a.v[1], b.v[1]);
        out.v[2] = max(a.v[2], b.v[2]);
        return out;
}
__host__ __device__ vec3f vecMin(const vec3f &a, const vec3f &b) {
        vec3f out;
       out.v[0] = min(a.v[0], b.v[0]);
        out.v[1] = min(a.v[1], b.v[1]);
        out.v[2] = min(a.v[2], b.v[2]);
        return out;
}

__host__ __device__
void
printVec(vec3f &a){
    printf("Vec:  %f %f %f\n",
            a.v[0], a.v[1], a.v[2]);
};

}

#endif
