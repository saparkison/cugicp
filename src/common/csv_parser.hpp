#ifndef __CSV_PARSER_HPP__
#define __CSV_PARSER_HPP__

#include <common/csv_parser.h>

namespace gicp {

csv_parser::csv_parser(const std::string & file_name): _m_file_name(file_name) { };


float * csv_parser::parseToArray(int *size) {
    std::vector<std::vector<float>> nodes;
	std::ifstream file(_m_file_name);

	std::size_t index = 0;
	std::string line;
	while (getline(file, line)) {
        std::vector<float> point;
		std::stringstream ss(line);

		std::string value_string;
		while (getline(ss, value_string, ',')) {
			float value = static_cast<float>(stold(value_string));
			point.push_back(value);
		}
        nodes.push_back(point);

	}
    size[0] = nodes.size();

	file.close();
    float *output = (float *)malloc(sizeof(float)*nodes.size()*nodes[1].size());
    int ita =0;
    for(auto a:nodes){
        int itb =0;
        for(auto b:a){
            output[ita*a.size()+itb] = b;
            itb++;
        }
        ita++;
    }
    return output;
}

}
#endif /* __CSV_PARSER_HPP__ */
