#ifndef __EIGENDECOMP__H_
#define __EIGENDECOMP__H_

#include<stdio.h>
#include<math.h>

#include<common/mat3f.h>
#include<common/vec3f.h>

// Adapted from Stan Melax's code
// source: http://melax.github.io/diag.html

namespace gicp {

__host__ __device__ __forceinline__
mat3f jacobiRot(const int &k, const int &l, const float &c, const float &s) {
    mat3f out;
    out.set(0.0);
    out.m[0][0] = 1.0;
    out.m[1][1] = 1.0;
    out.m[2][2] = 1.0;

    out.m[k][k] = c;
    out.m[l][l] = c;

    out.m[l][k] = s;
    out.m[k][l] = -s;

    return out;
}

__host__ __device__ __forceinline__
void  eigenDecomp(const mat3f &in, mat3f &eigVals, mat3f &eigVecs) {
	// A must be a symmetric matrix.
	// returns quaternion q such that its corresponding matrix Q
	// can be used to Diagonalize A
	// Diagonal matrix D = Q * A * Transpose(Q);  and  A = QT*D*Q
	// The rows of q are the eigenvectors D's diagonal is the eigenvalues
	// As per 'row' convention if float3x3 Q = q.getmatrix(); then v*Q = q*v*conj(q)
	int maxsteps=24;  // certainly wont need that many.
	int i;
	float q[4] = {0,0,0,1};
    mat3f Q; // v*Q == q*v*conj(q)
    Q.set(0.0);
    Q.m[0][0] = 1.0;
    Q.m[1][1] = 1.0;
    Q.m[2][2] = 1.0;
    mat3f D;
	for(i=0;i < maxsteps;i++)
	{
		D  = matrix_multiply(matrix_multiply(Q,in),trans(Q));  // A = Q^T*D*Q
		vec3f om, offdiag;
        offdiag.v[0] = D.m[1][2]; offdiag.v[1] =  D.m[0][2]; offdiag.v[2] = D.m[0][1]; 
        // elements not on the diagonal
        om.v[0] = fabs(D.m[1][2]); om.v[1] = fabs(D.m[0][2]); om.v[2] = fabs(D.m[0][1]); 
        // mag of each offdiag elem
        int k = biggestAxis(om);
		int k1 = (k+1)%3;
		int k2 = (k+2)%3;
		if(om.v[k]==0.0f) {
            break;  // diagonal already
        }
		float thet = (D.m[k2][k2]-D.m[k1][k1])/(2.0f*offdiag.v[k]);
		float sgn = (thet > 0.0f)?1.0f:-1.0f;
		thet    *= sgn; // make it positive
		float t = sgn /(thet +(sqrt(pow(thet,2)+1.0f))) ; // sign(T)/(|T|+sqrt(T^2+1))
		float c = 1.0f/sqrt(pow(t,2)+1.0f); //  c= 1/(t^2+1) , t=s/c
		if(c==1.0f) {
            break;  // no room for improvement - reached machine precision.
        }
        float s = c*t;
        mat3f J = jacobiRot(k1,k2, c, s);//quaternion(jr[0], jr[1], jr[2], jr[3]);
		Q =  matrix_multiply(J,Q);
	}

    eigVals.set(0.0);
    eigVals.m[0][0]  = D.m[0][0];
    eigVals.m[1][1]  = D.m[1][1];
    eigVals.m[2][2]  = D.m[2][2];
    eigVecs = Q;
	return;
};

}

#endif /* __EIGENDECOMP_H__ */
