#ifndef __CSV_PARSER_H__
#define __CSV_PARSER_H__

#include <fstream>
#include <sstream>
#include <string>
#include <vector>
//#include <vector>


namespace gicp {

class csv_parser {
public:
	csv_parser(const std::string & file_name);

    float * parseToArray(int *size);
private:
	std::string _m_file_name;
};

}

#endif /* __CSV_PARSER_H__ */
