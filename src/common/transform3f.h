#ifndef __TRANSFORM3F_H__
#define __TRANSFORM3F_H__

#include<common/vec3f.h>
#include<common/mat3f.h>

#include<math.h>
#include<stdio.h>

namespace gicp {

struct transform3f {
    float r[3];
    float t[3];
    float m[4][4];

    __host__ __device__ transform3f() {
        r[0] = 0;
        r[1] = 0;
        r[2] = 0;

        t[0] = 0;
        t[1] = 0;
        t[2] = 0;

        m[0][0] = 1;
        m[0][1] = 0;
        m[0][2] = 0;
        m[0][3] = 0;
        m[1][0] = 0;
        m[1][1] = 1;
        m[1][2] = 0;
        m[1][3] = 0;
        m[2][0] = 0;
        m[2][1] = 0;
        m[2][2] = 1;
        m[2][3] = 0;
        m[3][0] = 0;
        m[3][1] = 0;
        m[3][2] = 0;
        m[3][3] = 1;
    }


    __host__ __device__  __forceinline__
    transform3f(const float v[6]) {
        r[0] = v[0];
        r[1] = v[1];
        r[2] = v[2];
        t[0] = v[3];
        t[1] = v[4];
        t[2] = v[5];

        float sr = sin(r[0]); float cr = cos(r[0]);
        float sp = sin(r[1]); float cp = cos(r[1]);
        float sh = sin(r[2]); float ch = cos(r[2]);

        m[0][0] = ch*cp;
        m[0][1] = -sh*cr + ch*sp*sr;
        m[0][2] = sh*sr + ch*sp*cr;
        m[1][0] = sh*cp;
        m[1][1] = ch*cr + sh*sp*sr;
        m[1][2] = -ch*sr + sh*sp*cr;
        m[2][0] = -sp;
        m[2][1] = cp*sr;
        m[2][2] = cp*cr;

        m[0][3] = t[0];
        m[1][3] = t[1];
        m[2][3] = t[2];

        m[3][0] = 0;
        m[3][1] = 0;
        m[3][2] = 0;
        m[3][3] = 1;
    };

    __host__ __device__ __forceinline__
    vec3f operator()(const vec3f &in) {
        vec3f out;
        out.v[0] = in.v[0]*m[0][0] + in.v[1]*m[0][1] + in.v[2]*m[0][2] + m[0][3];
        out.v[1] = in.v[0]*m[1][0] + in.v[1]*m[1][1] + in.v[2]*m[1][2] + m[1][3];
        out.v[2] = in.v[0]*m[2][0] + in.v[1]*m[2][1] + in.v[2]*m[2][2] + m[2][3];
        return out;
    };

    __host__ __device__  __forceinline__
    mat3f operator()(const mat3f &in) {
        mat3f out, temp;
        for(int i = 0; i<3; i++) {
            for(int j = 0; j<3; j++) {
                temp.m[i][j] = 0;
                for(int k = 0; k<3; k++){
                    temp.m[i][j] += m[i][k]*in.m[k][j];
                }
            }
        }
        for(int i = 0; i<3; i++) {
            for(int j = 0; j<3; j++) {
                out.m[i][j] = 0;
                for(int k = 0; k<3; k++){
                    out.m[i][j] += temp.m[i][k]*m[j][k];
                }
            }
        }
        return out;
    }

    __host__ __device__ __forceinline__
    void transform(const transform3f &t) {
        transform3f temp;
        for(int i = 0; i<4; i++) {
            for( int j = 0; j<4; j++) {
                temp.m[i][j] = 0;
                for( int k = 0; k<4; k++) {
                    temp.m[i][j]+= t.m[i][k]*m[k][j];
                }
            }
        }
        *this = temp;
    }

    
    __host__ __device__  void copy(const transform3f &other) {
        r[0] = other.r[0];
        r[1] = other.r[1];
        r[2] = other.r[2];

        t[0] = other.t[0];
        t[1] = other.t[1];
        t[2] = other.t[2];

        m[0][0] = other.m[0][0];
        m[0][1] = other.m[0][1];
        m[0][2] = other.m[0][2];
        m[0][3] = other.m[0][3];
        m[1][0] = other.m[1][0];
        m[1][1] = other.m[1][1];
        m[1][2] = other.m[1][2];
        m[1][3] = other.m[1][3];
        m[2][0] = other.m[2][0];
        m[2][1] = other.m[2][1];
        m[2][2] = other.m[2][2];
        m[2][3] = other.m[2][3];
        m[3][0] = other.m[3][0];
        m[3][1] = other.m[3][1];
        m[3][2] = other.m[3][2];
        m[3][3] = other.m[3][3];
    }

    __host__ __device__ void printMat() {
        printf("t: ");
        for(int i =0; i<3; i++) {
            for(int j = 0; j < 4; j++) {
                printf("%f ",m[i][j]);
            }
        }
        printf("\n");
    }
};

__host__ __device__ __forceinline__
vec3f
compute_dr(const mat3f &dMatR, const float r[3]){
    mat3f dr, dp, dh;
    float sr = sin(r[0]); float cr = cos(r[0]);
    float sp = sin(r[1]); float cp = cos(r[1]);
    float sh = sin(r[2]); float ch = cos(r[2]);

    dr.m[0][0] = 0;
    dr.m[0][1] = sh*sr + ch*sp*cr;
    dr.m[0][2] = sh*sr + ch*sp*cr;
    dr.m[1][0] = 0;
    dr.m[1][1] = -ch*sr + sh*sp*cr;
    dr.m[1][2] = -ch*cr - sh*sp*sr;
    dr.m[2][0] = 0;
    dr.m[2][1] = cp*cr;
    dr.m[2][2] = -cp*sr;


    dp.m[0][0] = -ch*sp;
    dp.m[0][1] = ch*cp*sr;
    dp.m[0][2] = ch*cp*cr;
    dp.m[1][0] = -sh*sp;
    dp.m[1][1] = sh*cp*sr;
    dp.m[1][2] = sh*cp*cr;
    dp.m[2][0] = -cp;
    dp.m[2][1] = -sp*sr;
    dp.m[2][2] = -sp*cr;

    dh.m[0][0] = -sh*cp;
    dh.m[0][1] = -ch*cr - sh*sp*sr;
    dh.m[0][2] = ch*sr - sh*sp*cr;
    dh.m[1][0] = ch*cp;
    dh.m[1][1] = -sh*cr + ch*sp*sr;
    dh.m[1][2] = sh*sr + ch*sp*cr;
    dh.m[2][0] = 0;
    dh.m[2][1] = 0;
    dh.m[2][2] = 0;

    vec3f out;
    out.v[0] = trace(matrix_multiply(trans(dr), dMatR));
    out.v[1] = trace(matrix_multiply(trans(dp), dMatR));
    out.v[2] = trace(matrix_multiply(trans(dh), dMatR));
    return out;
}
}

#endif
