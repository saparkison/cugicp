#ifndef __MAT3F_H__
#define __MAT3F_H__

#include<stdio.h>

namespace gicp {

struct mat3f {
    float m[3][3];

    __host__ __device__ __forceinline__
    void set(const float &x){
        m[0][0] = x;
        m[0][1] = x;
        m[0][2] = x;
        m[1][0] = x;
        m[1][1] = x;
        m[1][2] = x;
        m[2][0] = x;
        m[2][1] = x;
        m[2][2] = x;
    };
    __host__ __device__ __forceinline__
    void scalar_mult(const float &s){
        m[0][0] *= s;
        m[0][1] *= s;
        m[0][2] *= s;
        m[1][0] *= s;
        m[1][1] *= s;
        m[1][2] *= s;
        m[2][0] *= s;
        m[2][1] *= s;
        m[2][2] *= s;
    };
    __host__ __device__ __forceinline__
    mat3f & operator=(const mat3f &other) {
        m[0][0] = other.m[0][0];
        m[0][1] = other.m[0][1];
        m[0][2] = other.m[0][2];
        m[1][0] = other.m[1][0];
        m[1][1] = other.m[1][1];
        m[1][2] = other.m[1][2];
        m[2][0] = other.m[2][0];
        m[2][1] = other.m[2][1];
        m[2][2] = other.m[2][2];
        return *this;
    }
};
__host__ __device__ __forceinline__
mat3f operator-(const mat3f &a, const mat3f &b) {
    mat3f out;
    out.m[0][0] = a.m[0][0] - b.m[0][0];
    out.m[0][1] = a.m[0][1] - b.m[0][1];
    out.m[0][2] = a.m[0][2] - b.m[0][2];
    out.m[1][0] = a.m[1][0] - b.m[1][0];
    out.m[1][1] = a.m[1][1] - b.m[1][1];
    out.m[1][2] = a.m[1][2] - b.m[1][2];
    out.m[2][0] = a.m[2][0] - b.m[2][0];
    out.m[2][1] = a.m[2][1] - b.m[2][1];
    out.m[2][2] = a.m[2][2] - b.m[2][2];
    return out;
};
__host__ __device__ __forceinline__
mat3f operator+(const mat3f &a, const mat3f &b) {
    mat3f out;
    out.m[0][0] = a.m[0][0] + b.m[0][0];
    out.m[0][1] = a.m[0][1] + b.m[0][1];
    out.m[0][2] = a.m[0][2] + b.m[0][2];
    out.m[1][0] = a.m[1][0] + b.m[1][0];
    out.m[1][1] = a.m[1][1] + b.m[1][1];
    out.m[1][2] = a.m[1][2] + b.m[1][2];
    out.m[2][0] = a.m[2][0] + b.m[2][0];
    out.m[2][1] = a.m[2][1] + b.m[2][1];
    out.m[2][2] = a.m[2][2] + b.m[2][2];
    return out;
};

__host__ __device__ __forceinline__
void matInv(mat3f &m){
    mat3f result, temp;
    temp.set(0.01);
    m = m+temp;
    float determinant = m.m[0][0]*(m.m[1][1]*m.m[2][2]-m.m[2][1]*m.m[1][2])
                        -m.m[0][1]*(m.m[1][0]*m.m[2][2]-m.m[1][2]*m.m[2][0])
                        +m.m[0][2]*(m.m[1][0]*m.m[2][1]-m.m[1][1]*m.m[2][0]);
    float invdet = 1/determinant;
    result.m[0][0] =  (m.m[1][1]*m.m[2][2]-m.m[2][1]*m.m[1][2])*invdet;
    result.m[1][0] = -(m.m[0][1]*m.m[2][2]-m.m[0][2]*m.m[2][1])*invdet;
    result.m[2][0] =  (m.m[0][1]*m.m[1][2]-m.m[0][2]*m.m[1][1])*invdet;
    result.m[0][1] = -(m.m[1][0]*m.m[2][2]-m.m[1][2]*m.m[2][0])*invdet;
    result.m[1][1] =  (m.m[0][0]*m.m[2][2]-m.m[0][2]*m.m[2][0])*invdet;
    result.m[2][1] = -(m.m[0][0]*m.m[1][2]-m.m[1][0]*m.m[0][2])*invdet;
    result.m[0][2] =  (m.m[1][0]*m.m[2][1]-m.m[2][0]*m.m[1][1])*invdet;
    result.m[1][2] = -(m.m[0][0]*m.m[2][1]-m.m[2][0]*m.m[0][1])*invdet;
    result.m[2][2] =  (m.m[0][0]*m.m[1][1]-m.m[1][0]*m.m[0][1])*invdet;
    m = result;
};

__host__ __device__ __forceinline__
mat3f trans(const mat3f &a) {
    mat3f out;
    out.m[0][0] = a.m[0][0];
    out.m[0][1] = a.m[1][0];
    out.m[0][2] = a.m[2][0];
    out.m[1][0] = a.m[0][1];
    out.m[1][1] = a.m[1][1];
    out.m[1][2] = a.m[2][1];
    out.m[2][0] = a.m[0][2];
    out.m[2][1] = a.m[1][2];
    out.m[2][2] = a.m[2][2];
    return out;
}

__host__ __device__ __forceinline__
mat3f matrix_multiply(const mat3f &a, const mat3f &b) {
    mat3f out;
    for(int i=0; i<3; i++) {
        for(int j=0; j<3; j++) {
            out.m[i][j] = 0;
            for(int k=0; k<3; k++) {
                out.m[i][j] += a.m[i][k]*b.m[k][j];
            }
        }
    }
    return out;
}

__host__ __device__ __forceinline__
float trace(const mat3f &a) {
    return a.m[0][0] +a.m[1][1] + a.m[2][2];
}

struct addInv{
    __host__ __device__ mat3f operator()(mat3f a, mat3f b) {
        mat3f out = a+b;
        matInv(out);
        return out;
    }
};

__host__ __device__
void printMat(const mat3f &a){
    printf("Mat3f:\n %f, %f, %f\n %f, %f, %f\n %f, %f, %f\n",
            a.m[0][0], a.m[0][1], a.m[0][2],
            a.m[1][0], a.m[1][1], a.m[1][2],
            a.m[2][0], a.m[2][1], a.m[2][2]);
}

}
#endif
