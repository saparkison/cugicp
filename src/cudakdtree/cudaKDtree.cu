#define THREADNUM 128

#define CUDA_STACK 50

#define LARGEN 256
#define LOG2N 8

#include<stdio.h>
#include<stdlib.h>
#include<float.h>
#include<curand.h>
#include<curand_kernel.h>

#define cudaSafe(ans) { gpuAssert((ans), __FILE__, __LINE__, true); }

#include<math.h>
#include<iostream>
#include<algorithm>
#include<chrono>

#include <thrust/execution_policy.h>
#include <thrust/device_vector.h>
#include <thrust/swap.h>
#include <thrust/sort.h>
#include <thrust/distance.h>
#include <thrust/system/system_error.h>

#include"cudaKDtree.h"

namespace gicp {

void gpuAssert(cudaError_t code, const char *file, int line, bool abort) {
    if (code != cudaSuccess) {
        fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
        if (abort) exit(code);
    }
}

struct MinMaxVec {
  vec3f min;
  vec3f max;
};

struct MinMaxUnary {
  __host__ __device__
    MinMaxVec operator()(const vec3f &in){
      MinMaxVec v;
      v.min = in;
      v.max = in;
      return v;
    }
};

struct MinMaxBinary {
  __host__ __device__
    MinMaxVec operator()(const MinMaxVec &a, const MinMaxVec &b) {
      MinMaxVec result;
      result.min = vecMin(a.min, b.min);
      result.max = vecMax(a.max, b.max);
      return result;
    }
};


struct isLess{
    float value;
    int axis;
    __host__ __device__ isLess(const float &f, const int &a) {
        value = f;
        axis = a;
    }

    __host__ __device__ bool operator()(const vec3f &a) {
        return a.v[axis]<value;
    }
};

struct isGreater{
    float value;
    int axis;
    __host__ __device__ isGreater(const float &f, const int &a) {
        value = f;
        axis = a;
    }

    __host__ __device__ bool operator()(const vec3f &a) {
        return a.v[axis]>value;
    }
};
struct sortByAxis{
    int axis;
    __host__ __device__ sortByAxis(const int &a) {
        axis = a;
    }

    __host__ __device__ bool operator()(const vec3f &a, const vec3f &b) {
        return a.v[axis]<b.v[axis];
    }

};

__host__ __device__
cuda_p_queue::cuda_p_queue(){
    _m_max = -1;
    _m_size = -1;
    _m_top = -1;

}

__host__ __device__
cuda_p_queue::cuda_p_queue(const int &size){
    _m_max = 2e16;
    _m_size = size;
    _m_top = 0;

    _m_priorities[0] = _m_max;
}

__host__ __device__
cuda_p_queue::~cuda_p_queue(){
}

__host__ __device__ void
cuda_p_queue::push(const float &p, const int &v){
    int insert = -1;
    for(int it = _m_top; it >= 0; it--){
        if(p<_m_priorities[it]){
            if(it+1<_m_size){
                _m_priorities[it+1] = _m_priorities[it];
                _m_values[it+1] = _m_values[it];
            }
            insert = it;
        }
    }
    if(insert!=-1){
        _m_priorities[insert] = p;
        _m_values[insert] = v;
        _m_top += 1;
    }
    if(_m_top==_m_size){
        _m_top--;
        _m_max = _m_priorities[_m_top];
    }

}


template<typename DerivedPolicy,
         typename RandomAccessIterator,
         typename StrictWeakOrdering>
__host__ __device__ void
nth_element(const thrust::detail::execution_policy_base< DerivedPolicy > &
    exec,
    RandomAccessIterator first,
    RandomAccessIterator nth,
    RandomAccessIterator last,
    StrictWeakOrdering comp) {

  if(nth == last) return;

  size_t dist= thrust::distance(first,last);

  while(dist>16) {
    thrust::swap(*nth, *(last-1));

    RandomAccessIterator pivot = thrust::partition(exec, first, last-1,
        isLess((last-1)->v[comp.axis], comp.axis));

    thrust::swap(*pivot, *(last-1));

    if(nth == pivot) break;

    if(thrust::distance(first, nth) < thrust::distance(first, pivot)) {
      last = pivot;
    } else {
      first = pivot;
    }
    dist = thrust::distance(first,last);
  }
  thrust::sort(exec, first, last, comp);
}

__device__ __forceinline__ int
computeVar(vec3f *d_points, const int &start, const int &stop, const int &k){
    float sum[3] = {0};
    float sqSum[3] = {0};
    float var[3] = {0};
    for(int i = start; i<=stop; i++){
        for(int j = 0; j<k; j++){
            sum[j]+=d_points[i].v[j];
            sqSum[j]+=(d_points[i].v[j]*d_points[i].v[j]);
        }
    }
    int n = stop+1-start;
    int maxIdx = -1;
    float max = 0;
    for(int j=0; j<k; j++){
        var[j]=(sqSum[j]-(sum[j]*sum[j])/n)/(n-1);
        if(var[j]>max){
            max = var[j];
            maxIdx = j;
        }
    }
    return maxIdx;
}

__device__ __forceinline__ int
partition(vec3f *d_points, const int &start, const int &stop, const int &pivot,
    const int &k, const int &axis){
    float pivotVal = d_points[pivot].v[axis];
    int storeIdx = 0;
    for(int i = start; i<=stop; i++){
        if(d_points[i].v[axis]<pivotVal){
          thrust::swap(d_points[storeIdx+start], d_points[i]);
            storeIdx++;
        }
    }
    return start+storeIdx;
}

__device__ __forceinline__ void
qselect(vec3f *d_points, int start, int stop, const int &k,
    int n, const int &axis){
    curandState_t state;
    curand_init(0,0,0,&state);
    while(true){
    if(start==stop)
        return;
    int max = stop-start;
    int pivot = curand(&state)%max;
    thrust::swap(d_points[pivot+start],d_points[stop]);
    pivot = partition(d_points, start, stop-1, stop, k, axis);
    thrust::swap(d_points[pivot], d_points[stop]);

    if(n==pivot){
        return;
    } else if ( n< pivot) {
        stop = pivot - 1;
    } else {
        start = pivot + 1;
    }
    }
}

__global__ void 
smallNodeStage(vec3f *d_points, Results_t *d_results, Results_t *d_results_out,
               Node_t *d_tree, int k, int current_index, int n){

    int pos = blockIdx.x*blockDim.x+threadIdx.x;
    if (pos>=n) return;
    int id = current_index+pos;

    if(d_results[pos].left) {
      d_tree[d_results[pos].parrent_id].leftChild = id;
    }
    else {
      d_tree[d_results[pos].parrent_id].rightChild = id;
    }

    int start = d_results[pos].range.start;
    int stop = d_results[pos].range.stop;
    int axis = computeVar(d_points, start, stop, k);
    int median = ((stop+1.0-start)/2.0)+start;

    qselect(d_points,start,stop,k,median,axis);
    //nth_element(thrust::device,
    //            d_points + start,
    //            d_points + median,
    //            d_points + stop + 1,
    //            sortByAxis(axis));

    d_tree[id].point = median;
    d_tree[id].axis = axis;

    d_results_out[2*pos].parrent_id = id;
    d_results_out[2*pos].left = true;
    if(median!=stop){
        d_results_out[2*pos].range.start=start;
        d_results_out[2*pos].range.stop=median-1;
    } else {
        d_results_out[2*pos].range.start = -1;
        d_results_out[2*pos].range.stop = -1;
    }

    d_results_out[2*pos+1].parrent_id = id;
    d_results_out[2*pos+1].left = false;
    if(stop!=median){
        d_results_out[2*pos+1].range.start = median+1;
        d_results_out[2*pos+1].range.stop = stop;
    } else {
        d_results_out[2*pos+1].range.start = -1;
        d_results_out[2*pos+1].range.stop = -1;
    }
}

__global__ void
largeNodeStage(vec3f *d_points, Results_t *d_results,
               Results_t *d_results_new, Node_t *d_tree, int p, int i) {
    int pos = blockIdx.x*blockDim.x+threadIdx.x;
    if (pos>=p) return;

    int treeIdx = pow(2,i)+pos-1;
    int start = d_results[pos].range.start;
    int stop = d_results[pos].range.stop;
    int median = ((stop+1-start)/2.0) + start;

    MinMaxVec startCnt;
    startCnt.min.set(FLT_MAX);
    startCnt.max.set(FLT_MIN);
    MinMaxVec result = thrust::transform_reduce(thrust::device,
                                d_points + start,
                                d_points + stop +1,
                                MinMaxUnary(),
                                startCnt,
                                MinMaxBinary());

    int axis = biggestAxis(result.max-result.min);
    int idLeft = pow(2,i+1)+pos*2-1;
    int idRight = pow(2,i+1)+pos*2;
    d_tree[treeIdx].point = median;
    d_tree[treeIdx].axis = axis;
    d_tree[treeIdx].leftChild = idLeft;
    d_tree[treeIdx].rightChild = idRight;

    d_results_new[2*pos].parrent_id = treeIdx;
    d_results_new[2*pos].left = true;
    d_results_new[2*pos].range.start = start;
    d_results_new[2*pos].range.stop = median-1;

    d_results_new[2*pos+1].parrent_id = treeIdx;
    d_results_new[2*pos+1].left = false;
    d_results_new[2*pos+1].range.start = median+1;
    d_results_new[2*pos+1].range.stop = stop;

    thrust::sort(thrust::device,
                 d_points + start,
                 d_points + stop + 1,
                 sortByAxis(axis));

    return;
}


__device__ __inline__ float
pDist(const vec3f &d_a, const vec3f &d_b){
    return (d_a.v[0]-d_b.v[0])*(d_a.v[0]-d_b.v[0])
           + (d_a.v[1]-d_b.v[1])*(d_a.v[1]-d_b.v[1])
           + (d_a.v[2]-d_b.v[2])*(d_a.v[2]-d_b.v[2]);
}

__device__ void
knns(vec3f *d_points, Node_t *d_tree, vec3f *query_point, int *closestP, int k, int k_nn){

    //printf("inside kernel\n");
    cuda_p_queue pq( (k_nn) );
    pq.set_value_ptr(closestP);
    float max = pq.get_max();
    //int top = 0;

    int visit_stack[CUDA_STACK];
    int visit_pos = 0;

    int back_stack[CUDA_STACK];
    int back_pos = 0;

    visit_stack[visit_pos++] = 0;

    vec3f &qp = *query_point;

    while(visit_pos>0 || back_pos>0){
        while(visit_pos>0){
            int currentNode = visit_stack[--visit_pos];
            if(currentNode<0)
                break;

            const int &nodePoint = d_tree[currentNode].point;
            const int &axis = d_tree[currentNode].axis;
            const int &leftNode = d_tree[currentNode].leftChild;
            const int &rightNode = d_tree[currentNode].rightChild;
            float currentDist = pDist(*(d_points+nodePoint), qp);

            if(currentDist<max){
                pq.push(currentDist, d_tree[currentNode].point);
                max = pq.get_max();
            }

            if(axis<0)
                break;

            if(qp.v[axis]<d_points[nodePoint].v[axis]){
                visit_stack[visit_pos++] = leftNode;
                back_stack[back_pos++] = currentNode;
            }
            else {
                visit_stack[visit_pos++] = rightNode;
                back_stack[back_pos++] = currentNode;
            }
        }

        if(back_pos>0){
            int currentNode = back_stack[--back_pos];

            if(currentNode>0){
                const int &nodePoint = d_tree[currentNode].point;
                const int &axis = d_tree[currentNode].axis;
                const int &leftNode = d_tree[currentNode].leftChild;
                const int &rightNode = d_tree[currentNode].rightChild;
                float axisDist = qp.v[axis]-d_points[nodePoint].v[axis];
                axisDist = axisDist*axisDist;

                if(axisDist<max){

                    if(qp.v[axis]<d_points[nodePoint].v[axis]){
                        visit_stack[visit_pos++] = rightNode;
                    }
                    else {
                        visit_stack[visit_pos++] = leftNode;
                    }
                }
            }
        }
    }
    return;
}


__global__ void
knn_search(vec3f *d_points, Node_t *d_tree, vec3f *queryPoints, int *nearestPoints, int n, int k, int k_nn){
    int pos =blockIdx.x*blockDim.x+threadIdx.x;
    if (pos>=n) return;

    knns(d_points, d_tree, queryPoints+pos, nearestPoints+pos*k_nn, k, k_nn);
    return;

}

cuda_kd_tree::cuda_kd_tree(){
    _m_d_tree_points = nullptr;
    _m_d_query_points = nullptr;

    _m_d_tree = nullptr;
    _m_d_nn = nullptr;
    _m_d_knn = nullptr;

    _m_k =0;
    _m_n_tree_points = 0;
    _m_n_query_points = 0;
}

cuda_kd_tree::cuda_kd_tree(int k){
    _m_d_tree_points = nullptr;
    _m_d_query_points = nullptr;

    _m_d_tree = nullptr;
    _m_d_nn = nullptr;
    _m_d_knn = nullptr;

    _m_k =k;
    _m_n_tree_points = 0;
    _m_n_query_points = 0;
}

cuda_kd_tree::~cuda_kd_tree(){
    if(_m_d_tree_points!=nullptr)
        cudaSafe(cudaFree(_m_d_tree_points));
    if(_m_d_tree!=nullptr)
        cudaSafe(cudaFree(_m_d_tree));
    if(_m_d_nn!=nullptr)
        cudaSafe(cudaFree(_m_d_nn));
    if(_m_d_knn!=nullptr)
        cudaSafe(cudaFree(_m_d_knn));
}

vec3f *
cuda_kd_tree::build_tree(vec3f *points, int n){
    _m_n_tree_points = n;

    cudaStream_t* stream = (cudaStream_t *)malloc(sizeof(cudaStream_t));
    cudaStreamCreate(stream);

    // Allocate and copy points
    cudaSafe( cudaMalloc((void **)&_m_d_tree_points, sizeof(vec3f)*n));
    cudaSafe(cudaMemcpyAsync(_m_d_tree_points, points, sizeof(vec3f)*n,
          cudaMemcpyHostToDevice, *stream));

    // Allocate and fill tree
    cudaSafe(cudaMalloc((void **)&_m_d_tree, sizeof(Node_t)*n));
    Node_t tempNode;
    tempNode.point = -1;
    tempNode.axis = -1;
    tempNode.leftChild = -1;
    tempNode.rightChild = -1;
    thrust::fill(thrust::device, _m_d_tree, _m_d_tree+n, tempNode);


    // Allocate and fill first result range
    Results_t temp_result;
    temp_result.range.start = 0;
    temp_result.range.stop = _m_n_tree_points - 1;
    Results_t *d_results, *d_new_results;

    cudaSafe(cudaMalloc((void **)&d_results, sizeof(Results_t)*n));
    cudaSafe(cudaMemcpyAsync(d_results, &temp_result, sizeof(Results_t),
          cudaMemcpyHostToDevice, *stream));
    cudaSafe(cudaMalloc((void **)&d_new_results, sizeof(Results_t)*n));



    // Large node stage, use many threads to sort
    for(int i=0; i<=LOG2N; i++) {
        int p = pow(2,i);
        int blocks = ceil((float)p/8.0);
        //if(_m_profile) {
        //auto t1 = std::chrono::high_resolution_clock::now();
        //std::cout << "P: " << p << " i: " << i << std::endl;}
        cudaSafe(cudaDeviceSynchronize());
        largeNodeStage<<<blocks,8>>>(_m_d_tree_points, d_results,
                d_new_results, _m_d_tree, p, i);
        cudaSafe(cudaDeviceSynchronize());
        thrust::copy(thrust::device, d_new_results, d_new_results + 2*p,
            d_results);
        //if(_m_profile) {
        //auto t2 = std::chrono::high_resolution_clock::now();
        //std::cout << "large node stage step: " << "time "<< 
        //std::chrono::duration_cast<std::chrono::microseconds>(t2-t1).count()
        //<<std::endl;}
    }

    int current_index = LARGEN*4-1;
    int n_active=LARGEN*2;

    // Small Node stage, each thread sorts their own region
    while(n_active>0){
        int blocks = ceil((float)n_active/8.0);
        //if(_m_profile) {
        //auto t1 = std::chrono::high_resolution_clock::now();
        //std::cout << "Active Nodes: " << n_active << " Blocks " << blocks <<" Total Nodes: " << n << std::endl;
        //}
        //medianSplit<<<1,1>>>(_m_d_tree_points, d_nodeRanges,d_newRanges, d_active, _m_d_tree, _m_k, d_nActiveNodes);
        smallNodeStage<<<blocks,8>>>(_m_d_tree_points, d_results,d_new_results,
            _m_d_tree, _m_k, current_index, n_active);
        //if(_m_profile) {
        //auto t2 = std::chrono::high_resolution_clock::now();
        //std::cout << "small node stage step: " << "time "<< 
        //std::chrono::duration_cast<std::chrono::microseconds>(t2-t1).count()
        //<<std::endl;}
        current_index += n_active;
        cudaSafe(cudaDeviceSynchronize());
        //if(_m_profile) {
        //auto t3 = std::chrono::high_resolution_clock::now();}
        //cleanUp<<<1,1>>>(d_nodeRanges,d_newRanges, d_active, _m_d_tree, d_nActiveNodes, d_curretnIndex);
        //cudaSafe(cudaDeviceSynchronize());
        //cudaMemcpy(&nActiveNodes,d_nActiveNodes, sizeof(int), cudaMemcpyDeviceToHost);
        Results_t *last_active = thrust::copy_if(thrust::device, d_new_results,
            d_new_results + 2* n_active, d_results, isActive());
        n_active = thrust::distance(d_results, last_active);
        //if(_m_profile) {
        //auto t4 = std::chrono::high_resolution_clock::now();
        //std::cout << "Clean up step: " << "time "<<
        //std::chrono::duration_cast<std::chrono::microseconds>(t4-t3).count()
        //<<std::endl; }
    }
    cudaSafe(cudaFree(d_results));
    cudaSafe(cudaFree(d_new_results));
    free(stream);

    return _m_d_tree_points;
}

int *
cuda_kd_tree::find_nn(vec3f *queryPoints, int n){

    return find_knn(queryPoints, n, 1);

}

int *
cuda_kd_tree::find_knn(vec3f *queryPoints, int n, int k){
    _m_n_query_points = n;

    _m_d_query_points = queryPoints;

    //std::cout << "Here\n";
    if(_m_d_knn!=nullptr)
        cudaSafe(cudaFree(_m_d_knn));
    cudaSafe(cudaMalloc((void **)&_m_d_knn, sizeof(int)*_m_n_query_points*k));

    int blocks = ceil((float)_m_n_query_points/THREADNUM);
    knn_search<<<blocks,THREADNUM>>>(_m_d_tree_points, _m_d_tree, _m_d_query_points,
        _m_d_knn, _m_n_query_points, _m_k, k);
    cudaSafe(cudaDeviceSynchronize());

    return _m_d_knn;

}

}
