#ifndef __CUDA_KD_TREE_H__
#define __CUDA_KD_TREE_H__

#include<common/vec3f.h>

namespace gicp {

struct Node_t {

    int point;
    int axis;
    int leftChild;
    int rightChild;

};

struct Range_t {
    int start;
    int stop;
};

struct Results_t {
  int parrent_id;
  Range_t range;
  bool left;
};

struct isActive {
  __host__ __device__
  bool operator()(const Results_t r)
  {
    return r.range.start>-1;
  }
};

class cuda_p_queue {
    private:
        float _m_max;
        int _m_size;
        int _m_top;

        float _m_priorities[20];
        int *_m_values;

    public:
        __host__ __device__ cuda_p_queue();
        __host__ __device__ cuda_p_queue(const int &size);

        __host__ __device__ ~cuda_p_queue();


        __host__ __device__ void push(const float &p, const int &v);


        __host__ __device__ void set_value_ptr(int *val){
          _m_values = val;
        }

        __host__ __device__ float get_max(){
            return _m_max;
        }
        __host__ __device__ int *get_values(){
            return _m_values;
        }
        __host__ __device__ float *get_priorities(){
            return _m_priorities;
        }
        __host__ __device__ int get_top(){
            return _m_top;
        };
};


class cuda_kd_tree {
    private:
        vec3f *_m_d_tree_points;
        vec3f *_m_d_query_points;

        Node_t *_m_d_tree;
        int *_m_d_nn;
        int *_m_d_knn;

        int _m_k;
        int _m_n_tree_points;
        int _m_n_query_points;

        bool _m_profile = false;

    public:
        cuda_kd_tree();
        cuda_kd_tree(int k);

        ~cuda_kd_tree();

        int *find_nn(vec3f *d_queryPoints, int n);
        int *find_knn(vec3f *d_queryPoints, int n, int k);
        vec3f *build_tree(vec3f *points, int n);

};

}


#endif /* __CUDA_KD_TREE_H__ */
