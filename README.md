# About

**Name:** cugicp

**Summary:** A Parallelized algorithm to solve the Generalized ICP cost function

**Description:** A Parallelized algorithm to solve the Generalized ICP cost function 


**License:** BSD 3-Clause

# Requirements


## libraries
* CUDA
* Thrust
* Ceres-Solver
* gtest

# Install

**Local:** `make`. The project will be installed to `build/`.

**System-wide:** `make BUILD_PREFIX=/usr/local` as root.

# Maintainers

Steven Parkison (sparki@umich.edu), Arash Ushani (aushani@umich.edu), Sudhanva
Sreesha (ssreesha@umich.edu)
